/* {{{ irc-seven: Cows like it.
 *
 * Copyright (C) 1990 Jarkko Oikarinen and University of Oulu, Co Center.
 * Copyright (C) 1996-2002 Hybrid Development Team.
 * Copyright (C) 2002-2005 ircd-ratbox development team.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to:
 *
 *	Free Software Foundation, Inc.
 *	51 Franklin St - Fifth Floor
 *	Boston, MA 02110-1301
 *	USA
 *
 * }}} */

/* {{{ Includes. */
#include "stdinc.h"
#include "client.h"
#include "irc_string.h"
#include "ircd.h"
#include "numeric.h"
#include "send.h"
#include "msg.h"
#include "parse.h"
#include "modules.h"
#include "s_conf.h"
#include "s_serv.h"
#include "packet.h"
/* }}} */

static int m_away (struct Client *, struct Client *, int, const char **);

/* {{{ static struct Message away_msgtab = { ... } */
static struct Message away_msgtab =
{
	"AWAY", 0, 0, 0, MFLG_SLOW,
	{
		mg_unreg, {m_away, 0}, {m_away, 0},
		mg_ignore, mg_ignore, {m_away, 0},
	},
};
/* }}} */

/* {{{ static mapi_clist_av1 away_clist[] = { ... } */
static mapi_clist_av1 away_clist[] =
{
	&away_msgtab,
	NULL
};
/* }}} */

/* {{{ DECLARE_MODULE_AV1(...) */
DECLARE_MODULE_AV1
(
	away,
	NULL,
	NULL,
	away_clist,
	NULL,
	NULL,
	"1.1"
);
/* }}} */

/* {{{ static int m_away()
 *
 * AWAY message handler.
 *
 * Usage:
 *	AWAY [<message>]
 *
 * parv[0] - prefix
 * parv[1] - message, or nothing
 */
static int
m_away (struct Client *client_p, struct Client *source_p, int parc, const char **parv)
{
	char *away = NULL;
	char *away2 = NULL;

	if (!IsClient(source_p))
		return 0;
	if (MyClient(source_p) && !IsFloodDone(source_p))
		flood_endgrace(source_p);

	away = source_p->user->away;
	if (parc < 2 || EmptyString(parv[1]))
	{
		if (away)
		{
			sendto_server(client_p, NULL, CAP_TS6, NOCAPS,
				":%s AWAY", use_id(source_p));
			sendto_server(client_p, NULL, NOCAPS, CAP_TS6,
				":%s AWAY", source_p->name);

			MyFree(away);
			source_p->user->away = NULL;
		}

		if (MyConnect(source_p))
			sendto_one(source_p, form_str(RPL_UNAWAY), me.name,
				source_p->name);

		return 0;
	}

	if (MyConnect(source_p))
	{
		if (!IsOper(source_p) &&
			(CurrentTime - source_p->localClient->last_away) < ConfigFileEntry.pace_wait)
		{
			sendto_one(source_p, form_str(RPL_LOAD2HI), me.name,
				source_p->name, "AWAY");
			return 0;
		}

		source_p->localClient->last_away = CurrentTime;
	}

	away2 = LOCAL_COPY(parv[1]);
	if (strlen(away2) > AWAYLEN)
		away2[AWAYLEN] = '\0';

	if (!away)
	{
		sendto_server(client_p, NULL, CAP_TS6, NOCAPS, ":%s AWAY :%s",
			use_id(source_p), away2);
		sendto_server(client_p, NULL, NOCAPS, CAP_TS6, ":%s AWAY :%s",
			source_p->name, away2);
	}
	else
		MyFree(away);

	DupString(away, away2);
	source_p->user->away = away;

	if (MyConnect(source_p))
		sendto_one(source_p, form_str(RPL_NOWAWAY), me.name,
			source_p->name);

	return 0;
}
/* }}} */

/*
 * vim: ts=8 sw=8 noet fdm=marker tw=80
 */
