/* {{{ irc-seven: Cows like it.
 *
 * Copyright (C) 2006 William Pitcock.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to:
 *
 *	Free Software Foundation, Inc.
 *	51 Franklin St - Fifth Floor
 *	Boston, MA 02110-1301
 *	USA
 *
 * }}} */

#include "stdinc.h"
#include "modules.h"
#include "client.h"
#include "ircd.h"

static int _modinit (void);
static void _moddeinit (void);
static int eb_extended (const char *, struct Client *, struct Channel *, long);

/* {{{ DECLARE_MODULE_AV1(...) */
DECLARE_MODULE_AV1
(
	extb_extended,
	_modinit,
	_moddeinit,
	NULL,
	NULL,
	NULL,
	"1.1"
);
/* }}} */

/* {{{ static int _modinit() */
static int
_modinit (void)
{
	extban_table['x'] = eb_extended;
	return 0;
}
/* }}} */

/* {{{ static void _moddeinit() */
static void
_moddeinit (void)
{
	extban_table['x'] = NULL;
}
/* }}} */

/* {{{ static int eb_extended() */
static int eb_extended (const char *data, struct Client *client_p,
	struct Channel *chptr, long mode_type)
{
	char	buf[BUFSIZE];
	int	ret;

	if (!data)
		return EXTBAN_INVALID;

	ircsnprintf(buf, BUFSIZE, "%s!%s@%s#%s", client_p->name, client_p->username,
		client_p->host, client_p->info);

	ret = match(data, buf) ? EXTBAN_MATCH : EXTBAN_NOMATCH;
	if (ret == EXTBAN_NOMATCH && IsDynSpoof(client_p))
	{
		ircsnprintf(buf, BUFSIZE, "%s!%s@%s#%s", client_p->name,
			client_p->username, client_p->orighost, client_p->info);

		ret = match(data, buf) ? EXTBAN_MATCH : EXTBAN_NOMATCH;
	}

	return ret;
}
/* }}} */

/*
 * vim: ts=8 sw=8 noet fdm=marker tw=80
 */
