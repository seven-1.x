/* {{{ irc-seven: Cows like it.
 *
 * Copyright (C) 2006 Jilles Tjoelker.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to:
 *
 *	Free Software Foundation, Inc.
 *	51 Franklin St - Fifth Floor
 *	Boston, MA 02110-1301
 *	USA
 *
 * }}} */

/* {{{ Includes. */
#include "stdinc.h"
#include "modules.h"
#include "client.h"
#include "channel.h"
#include "hash.h"
#include "ircd.h"
/* }}} */

static int _modinit (void);
static void _moddeinit (void);
static int eb_channel (const char *, struct Client *, struct Channel *, long);

/* {{{ DECLARE_MODULE_AV1(...) */
DECLARE_MODULE_AV1
(
	extb_channel,
	_modinit,
	_moddeinit,
	NULL,
	NULL,
	NULL,
	"1.1"
);
/* }}} */

/* {{{ static int _modinit() */
static int
_modinit (void)
{
	extban_table['c'] = eb_channel;
	return 0;
}
/* }}} */

/* {{{ static void _moddeinit() */
static void
_moddeinit (void)
{
	extban_table['c'] = NULL;
}
/* }}} */

/* {{{ static int eb_channel() */
static int eb_channel (const char *data, struct Client *client_p,
	struct Channel *chptr, long mode_type)
{
	struct Channel *target_ch = NULL;

	if (!data)
		return EXTBAN_INVALID;

	target_ch = find_channel(data);
	if (target_ch == NULL)
		return EXTBAN_INVALID;

	/* require consistent target */
	if (chptr->chname[0] == '#' && data[0] == '&')
		return EXTBAN_INVALID;

	/* privacy! don't allow +s/+p channels to influence another channel */
	if (!PubChannel(target_ch))
		return EXTBAN_INVALID;

	return IsMember(client_p, target_ch)
		? EXTBAN_MATCH
		: EXTBAN_NOMATCH;
}
/* }}} */

/*
 * vim: ts=8 sw=8 noet fdm=marker tw=80
 */
