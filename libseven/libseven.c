/* {{{ irc-seven: Cows like it.
 *
 * Copyright (C) 1990 Jarkko Oikarinen and University of Oulu, Co Center.
 * Copyright (C) 1996-2002 Hybrid Development Team.
 * Copyright (C) 2002-2005 ircd-ratbox development team.
 * Copyright (C) 2005 William Pitcock and Jilles Tjoelker.
 *
 * Based on mkpasswd.c, originally by Nelson Minar (minar@reed.edu).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to:
 *
 *	Free Software Foundation, Inc.
 *	51 Franklin St - Fifth Floor
 *	Boston, MA 02110-1301
 *	USA
 *
 * }}} */

#include "stdinc.h"
#include "libseven.h"

static seven_log_cb log_callback = NULL;
static seven_log_cb restart_callback = NULL;
static seven_log_cb die_callback = NULL;

static char errbuf[BUFSIZE * 2];

/* {{{ void libseven_log() */
void libseven_log(const char *str, ...)
{
	va_list args;

	if (log_callback == NULL)
		return;

	va_start(args, str);
	ircvsnprintf(errbuf, BUFSIZE * 2, str, args);
	va_end(args);

	log_callback(errbuf);
}
/* }}} */

/* {{{ void libseven_restart() */
void libseven_restart(const char *str, ...)
{
	va_list args;

	if (restart_callback == NULL)
		return;

	va_start(args, str);
	ircvsnprintf(errbuf, BUFSIZE * 2, str, args);
	va_end(args);

	restart_callback(errbuf);
}
/* }}} */

/* {{{ void libseven_die() */
void libseven_die(const char *str, ...)
{
	va_list args;

	if (die_callback == NULL)
		return;

	va_start(args, str);
	ircvsnprintf(errbuf, BUFSIZE * 2, str, args);
	va_end(args);

	die_callback(errbuf);
}
/* }}} */

/* {{{ void libseven_init() */
void libseven_init(seven_log_cb log_cb, seven_log_cb restart_cb, seven_log_cb die_cb)
{
	log_callback = log_cb;
	restart_callback = restart_cb;
	die_callback = die_cb;

	fdlist_init();
	init_netio();
	eventInit();
	initBlockHeap();
	init_dlink_nodes();
	linebuf_init();
}
/* }}} */

/*
 * vim: ts=8 sw=8 noet fdm=marker tw=80
 */
