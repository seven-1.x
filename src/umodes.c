/* {{{ irc-seven: Cows like it.
 *
 * Copyright (C) 1990 Jarkko Oikarinen and University of Oulu, Co Center
 * Copyright (C) 1996-2002 Hybrid Development Team
 * Copyright (C) 2002-2005 ircd-ratbox development team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to:
 *
 *	Free Software Foundation, Inc.
 *	51 Franklin St - Fifth Floor
 *	Boston, MA 02110-1301
 *	USA
 *
 * }}} */

#include "stdinc.h"
#include "client.h"

char umodebuf[128];

/* {{{ int user_modes[256] = { ... } */
int user_modes[256] =
{
	/* 0x00 .. 0x0f */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	/* 0x10 .. 0x1f */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	/* 0x20 .. 0x2f */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	/* 0x30 .. 0x3f */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	/* @ */			0,
	/* A */			0,
	/* B */			0,
	/* C */			0,
	/* D */			UMODE_DEAF,
	/* E */			0,
	/* F */			0,
	/* G */			0,
	/* H */			0,
	/* I */			0,
	/* J */			0,
	/* K */			0,
	/* L */			0,
	/* M */			0,
	/* N */			0,
	/* O */			0,
	/* P */			0,
	/* Q */			UMODE_NOFORWARD,
	/* R */			UMODE_REGONLYMSG,
	/* S */			UMODE_SERVICE,
	/* T */			UMODE_HELPER,
	/* U */			0,
	/* V */			0,
	/* W */			0,
	/* X */			0,
	/* Y */			0,
	/* Z */			0,
	/* 0x5b .. 0x60 */	0, 0, 0, 0, 0, 0,
	/* a */			UMODE_ADMIN,
	/* b */			0,
	/* c */			0,
	/* d */			0,
	/* e */			0,
	/* f */			0,
	/* g */			UMODE_CALLERID,
	/* h */			0,
	/* i */			UMODE_INVISIBLE,
	/* j */			0,
	/* k */			0,
	/* l */			0,
	/* m */			0,
	/* n */			0,
	/* o */			UMODE_OPER,
	/* p */			UMODE_OVERRIDE,
	/* q */			0,
	/* r */			0,
	/* s */			UMODE_SERVNOTICE,
	/* t */			0,
	/* u */			0,
	/* v */			0,
	/* w */			UMODE_WALLOP,
	/* x */			0,
	/* y */			0,
	/* z */			UMODE_OPERWALL,
	/* 0x7b .. 0x7f */	0, 0, 0, 0, 0,
	/* 0x80 .. 0x8f */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	/* 0x90 .. 0x9f */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	/* 0xa0 .. 0xaf */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	/* 0xb0 .. 0xbf */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	/* 0xc0 .. 0xcf */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	/* 0xd0 .. 0xdf */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	/* 0xe0 .. 0xef */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	/* 0xf0 .. 0xff */	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
};
/* }}} */

/*
 * vim: ts=8 sw=8 noet fdm=marker tw=80
 */
